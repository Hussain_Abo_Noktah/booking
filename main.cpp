#include <iostream>
#include <algorithm>
#include <sstream>
using namespace std;

int adultCount, infantCount, childrenCount, maxGuest = 7, maxPaxTypePerRoom = 3, maxRoomPerBooking = 3;

int getRoomCount(int adults, int children, int infants) {

    int biggestPaxTypeCount = max(adults, max(children, infants));
    int roomCount = ceil( float(biggestPaxTypeCount) / float(maxPaxTypePerRoom));

    return roomCount;

}

string checkBookingRules(int adults, int children, int infants) {


    int guestCount = children + adults;

    if (guestCount > maxGuest) {
        return "In one booking, maximum guests can be 7 (excluding infants)";
    }
    int roomsCount = getRoomCount(adults, children, infants);

    if (roomsCount > adults) {
        return "room must be dont have only children or infants (i.e without at least one adult supervision)";

    } else if (roomsCount > maxRoomPerBooking) {
        return "The maximum number of rooms per booking  must be : 3";

    } else {
        stringstream out;
        out << "for " << adults << " adults , " << children << " children , "
            << infants << " infants :\n can be fit in " << roomsCount << "room/rooms";
        return  out.str();


    }
}

int main() {


    cout << "Please Enter number Of Adults : ";
    cin >> adultCount;

    cout << "Please Enter number Of Children : ";
    cin >> childrenCount;

    cout << "Please Enter number Of infant : ";
    cin >> infantCount;


    cout<<checkBookingRules(adultCount ,childrenCount , infantCount);
    return 0 ;


}
